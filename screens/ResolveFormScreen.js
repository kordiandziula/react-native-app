import React from 'react'
import { FormControl, NativeBaseProvider, Text, Box, Radio } from 'native-base'
import { View, Button } from 'react-native'
import ShowFormScreen from './ShowFormScreen';


const Question = (props) => {
    return (
        <>
            <Box
              alignItems="center"
              mt="1"
            >
                <FormControl.Label>
                    {props.question}
                </FormControl.Label>
                <Radio.Group
                  onChange= {(value) => {
                    props.setAnswer(props.questionId, value)
                  }}
                  accessibilityLabel="favorite number"
                >
                    {
                        props.answers.map((a, ai) => {
                            return (
                                <Radio 
                                  size = "sm"
                                  key={ai}
                                  value={a}
                                  my="1"
                                >
                                  {a}
                              </Radio>
                            )
                        })
                    }
                </Radio.Group>
            </Box>
        </>
    )
}

class ResolveFormScreen extends React.Component{

    constructor(props){
        super(props)   

        this.state = {
            forms: [],
            choosenForm: undefined,
            answers: undefined
        }
    }

    setChoosenForm = (formId) => {
        this.setState({
            choosenForm: this.state.forms[formId],
            answers: this.state.forms[formId].form.map((q) => {
                return {
                    question: q.question,
                    answer: undefined
                }
            })
        })
    }

    setAnswer = (questionId, answer) => {
        this.setState({
            answers: this.state.answers.map((a, ai) => {
                if (questionId === ai){
                    return {
                        question: a.question,
                        answer: answer
                    }
                }
                else{
                    return a
                }
            })
        })
    }

    resetState = () => {
        fetch("http://ug-app.azurewebsites.net").then(
            (res) => res.json()).then(
            (res) => {
                this.setState({
                    forms: res,
                    choosenForm: undefined,
                    answers: undefined
                })
            })
    }

    submit = async() => {

        const response = await fetch("https://ug-app.azurewebsites.net/resolve",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    form_id: this.state.choosenForm.id,
                    answers: this.state.answers
                })
            }
        )

        await this.resetState()
    }

    componentDidMount() {
        this.resetState()
    }

    render() {
        return (
            <NativeBaseProvider>
                {
                    this.state.choosenForm ? 
                    <>
                        <Box 
                          alignItems="center"
                          mt="5"
                        >
                            <FormControl.Label _text={{
                                bold: true
                            }}>
                                Nazwa:
                            </FormControl.Label>
                            <Text>
                                {this.state.choosenForm.name}
                            </Text>
                        </Box>
                        <Box
                          alignItems="center"
                          mt="5"
                        >
                            <FormControl.Label _text={{
                                bold: true
                            }}>
                                Pytania:
                            </FormControl.Label> 
                        </Box>

                        {
                            this.state.choosenForm.form.map((f, fi) => {
                                return <Question
                                         key = {fi}
                                         questionId = {fi}
                                         question = {f.question}
                                         answers = {f.answers}
                                         setAnswer = {this.setAnswer}
                                       />
                            })
                        }


                        <Box alignItems="center" mt="3">
                            <Button onPress={() => this.submit()} title="ZAPISZ" color="gray"/>
                        </Box>
                        
                        <Box alignItems="center" mt="3" mb="3">
                            <Button onPress={() => this.resetState()} title="ANULUJ" color="red"/>
                        </Box>
                    </>
                    :
                    <ShowFormScreen 
                        forms = {this.state.forms}
                        choose = {this.setChoosenForm}  
                    />
                }
            </NativeBaseProvider>
        )
    }
}

export default ResolveFormScreen