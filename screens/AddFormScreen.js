import React from 'react'
import { View, Button, TextInput, Text } from 'react-native'
import { Box, FormControl, Input, NativeBaseProvider, Divider, ScrollView, IconButton, AntDesign} from 'native-base'


const Question = props => {
    return (
        <Box 
          alignItems="center" 
          mt="5">
            <FormControl.Label>Pytanie:</FormControl.Label>
            <Input 
              type="text" 
              w="90%" 
              py="0" 
              InputRightElement={
                <Button 
                  size="xs" 
                  rounded="none" 
                  w="1/6" 
                  h="full" 
                  variant="outline"
                  color="gray" 
                  title="Usuń"
                  onPress={() => {props.deleteQuestion(props.questionId)}}
                />
              }
              value={props.question}
              onChange={(ev) => {props.setQuestion(props.questionId, ev.nativeEvent.text)}}
            />
        </Box>
    )
}

const Answer = props => {
    return (
        <Input 
          type="text" 
          w="90%" 
          py="0" 
          mt = "1"
          InputRightElement={
            <Button 
              size="xs" 
              rounded="none" 
              w="1/6" 
              h="full" 
              title="Usuń" 
              variant="outline"
              color="gray" 
              onPress={() => {props.deleteAnswer(props.questionId, props.answerId)}}
            />
          } 
          value = {props.answer}
          onChange = {(ev) => props.setAnswer(props.questionId, props.answerId, ev.nativeEvent.text)}
        />
    )
}

class AddFormScreen extends React.Component {

    constructor(props){
        super(props);
        
        if (props.state !== undefined){
            this.state = props.state
            this.resetState = props.resetState
            this.submit = props.submit
        }
        else{
            const initialState = {
                name: '',
                form: [
                    { question: '',
                      answers: [''] }
                ]
        
            }

            this.resetState = () => {
                this.setState(initialState)
            }

            this.state = initialState

            this.submit = async(form) => {
                const response = await fetch("https://ug-app.azurewebsites.net",
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        name: this.state.name,
                        form: form
                    })
                }
                )


                await this.resetState()
            }
        }
    }

    setName = (value) => {
        //console.log(value)
        this.setState({
            name: value,
            form: this.state.form
        })
    }

    setQuestion = (questionId, value) => {
        this.setState({
            form: this.state.form.map((el, i) => {
                if (i === questionId){
                    return { question: value,
                             answers: el.answers }
                }
                else{
                    return el
                }
            })
        })
    }

    setAnswer = (questionId, answerId, value) => {
        this.setState({
            form: this.state.form.map((el, i) => {
                if (i === questionId){
                    return { question: el.question,
                             answers: el.answers.map((a, ai) => {
                                if (ai === answerId) {
                                    return value
                                }
                                else {
                                    return a
                                }
                             }) 
                            }
                }
                else{
                    return el
                }
            })
        })
    }

    addQuestion = () => {
        this.setState({
            form: this.state.form.concat([{
                question: '',
                answers: ['']
            }])
        })
    }

    addAnswer = (questionId) => {
        this.setState({
            form: this.state.form.map((el, i) => {
                if (i === questionId){
                    return { question: el.question,
                             answers: el.answers.concat([''])
                           }
                }
                else{
                    return el
                }
            })
        })
    }

    deleteQuestion = (questionId) => {
        this.setState({
            form: this.state.form.filter((q, qi) => {
                if (qi === questionId){
                    return false
                }
                else{
                    return true
                }
            })
        })
    }

    deleteAnswer = (questionId, answerId) => {
        this.setState({
            form: this.state.form.map((el, i) => {
                if (i === questionId){
                    return { question: el.question,
                             answers: el.answers.filter((a, ai) => {
                                if (ai === answerId){
                                    return false
                                }
                                else{
                                    return true
                                }
                             })
                           }
                }
                else{
                    return el
                }
            })
        })
    }

    render() {
        return (
            <NativeBaseProvider>
                <ScrollView>
                    <Box 
                      alignItems="center" 
                      mt="5">
                        <FormControl.Label>Nazwa:</FormControl.Label>
                        <Input 
                          type="text" 
                          w="90%"
                          value={this.state.name}
                          onChange={(ev) => this.setName(ev.nativeEvent.text)}
                        />
                    </Box>
                    {
                        this.state.form.map((q, qi) => {
                            return ( 
                                <View key = {qi}>
                                    <Question 
                                        questionId = {qi}
                                        question = {q.question} 
                                        setQuestion = {this.setQuestion}
                                        deleteQuestion = {this.deleteQuestion}
                                    />
                                    <Box 
                                      alignItems="center" 
                                      mt="5">
                                        <FormControl.Label>Odpowiedzi:</FormControl.Label>
                                        {
                                            q.answers.map((a, ai) => {
                                                return (
                                                  <Answer 
                                                    key = {ai}
                                                    answer = {a}
                                                    questionId = {qi}
                                                    answerId = {ai}
                                                    setAnswer = {this.setAnswer}
                                                    deleteAnswer = {this.deleteAnswer}
                                                  />
                                                )
                                            })
                                        }
                                    </Box>
                                    <Box alignItems="center" mt="3">
                                        <Button onPress={() => this.addAnswer(qi)} title="DODAJ ODPOWIEDŹ" color="gray"/>
                                    </Box>

                                    <Divider mt = "3" />
                                </View>
                            )
                        })
                    }

                <Box alignItems="center" mt="3">
                    <Button onPress={() => this.addQuestion()} title="DODAJ PYTANIE" color="gray"/>
                </Box>

                <Box alignItems="center" mt="3">
                    <Button onPress={() => this.submit(this.state.form)} title="ZAPISZ" color="gray"/>
                </Box>

                <Box alignItems="center" mt="3" mb="3">
                    <Button onPress={() => this.resetState()} title="ANULUJ" color="red"/>
                </Box>

                </ScrollView>
            </NativeBaseProvider>
        )
    }
}

export default AddFormScreen

