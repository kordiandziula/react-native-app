import React from 'react'
import { NativeBaseProvider, Text } from 'native-base'

import ShowFormScreen from './ShowFormScreen';
import AddFormScreen from './AddFormScreen';

class EditFormScreen extends React.Component{
    
    resetState = () => {
        fetch("http://ug-app.azurewebsites.net/").then(
            (res) => res.json()).then(
            (res) => {
                this.setState({
                    forms: res,
                    choosenForm: undefined
                })
            })
    }
    
    constructor(){
        super();
        
        const initialState = {
            forms: [],
            choosenForm: undefined   
        }

        this.state = initialState

    }

    setChoosenForm = (formId) => {
        this.setState({
            choosenForm: this.state.forms[formId]
        })
    }

    submit = async(form) => {
        const response = await fetch("https://ug-app.azurewebsites.net/",
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                id: this.state.choosenForm.id,
                name: this.state.choosenForm.name,
                form: form
            })
        }
        )

        //console.log(response)

        await this.resetState()
    }

    componentDidMount = () => {
        this.resetState()
    }

    render() {
        return (
            <NativeBaseProvider>
                {
                    this.state.choosenForm ? 
                    <AddFormScreen 
                      state = {this.state.choosenForm} 
                      resetState = {this.resetState} 
                      submit = {this.submit}
                    />
                    :
                    <ShowFormScreen 
                        forms = {this.state.forms}
                        choose = {this.setChoosenForm}  
                    />
                }
            </NativeBaseProvider>
        )
    }
}

export default EditFormScreen;