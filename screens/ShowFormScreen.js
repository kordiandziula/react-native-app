import React from 'react'
import { Button } from 'react-native'
import { Box, VStack, Divider, NativeBaseProvider, ScrollView, Text } from 'native-base';

const Form = (props) => {
    return (
        <Box>
            <Box px="4" pt="4" alignItems="center">
              <Text mb="2" >{props.name}</Text>
              <Button size="xs" title="wybierz" color="gray" onPress={() => props.choose(props.formId)}/>
            </Box>
            <Divider mt="2" />
        </Box>
    )
} 

const ShowFormScreen = (props) => {
    return (
        <ScrollView>
            {
                props.forms.map((f, fi) => {
                    return <Form 
                                key = {fi}
                                name = {f.name}
                                formId = {fi}
                                choose = {props.choose}
                            />
                })
            }
        </ScrollView>
    )
}

export default ShowFormScreen