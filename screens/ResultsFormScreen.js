import React from 'react'
import { NativeBaseProvider, Text, View } from 'native-base'
import { Button } from 'react-native'
import { Box, VStack, Divider, ScrollView } from 'native-base';

import ShowFormScreen from './ShowFormScreen';

//props.question
//props.answers
const Question = (props) => {
    return (
        <>
            <Box px="4" pt="4">
              <Text mb="2" style={{fontWeight: 600}} >{props.question}</Text>

            {
                props.answers.map((a, ai) => {
                    return (
                        <View key={ai}>
                            <Text>
                                <Text style={{fontWeight: 600}}>Odpowiedz:</Text> {a.answer}
                            </Text>
                            <Text>
                                <Text style={{fontWeight: 600}}>Ilość:</Text> {a.cnt}
                            </Text>
                        </View>
                    )
                })
            }

                <Divider mt="2" />

            </Box>
        </>
    )
}

class ResultsFormScreen extends React.Component{
    
    resetState = () => {
        fetch("http://ug-app.azurewebsites.net/resolve").then(
            (res) => res.json()).then(
            (res) => {
                this.setState({
                    forms: res,
                    choosenForm: undefined
                })
            })
    }
    
    constructor(){
        super();
        
        const initialState = {
            forms: [],
            choosenForm: undefined   
        }

        this.state = initialState

    }

    setChoosenForm = (formId) => {
        this.setState({
            choosenForm: this.state.forms[formId]
        })
    }

    componentDidMount = () => {
        this.resetState()
    }

    render() {
        return (
            <NativeBaseProvider>
                <ScrollView>
                {
                    this.state.choosenForm ?
                    <>
                        <Box>
                            <Box px="4" pt="4" alignItems="center">
                              <Text mb="2">{this.state.choosenForm.name}</Text>
                            </Box>
                            <Divider mt="2" />

                            {
                                this.state.choosenForm.questions.map((q, qi) => {
                                    return (
                                        <Question
                                            key={qi}
                                            question={q.question}
                                            answers={q.answers}
                                        />
                                    )
                                })
                            }

                        </Box>
                    </>
                    :
                    <ShowFormScreen 
                        forms = {this.state.forms}
                        choose = {this.setChoosenForm}  
                    />
                }
                </ScrollView>
            </NativeBaseProvider>
        )
    }
}

export default ResultsFormScreen;