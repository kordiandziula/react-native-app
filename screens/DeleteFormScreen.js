import React from 'react'
import { NativeBaseProvider, Text } from 'native-base'

import ShowFormScreen from './ShowFormScreen';

class DeleteFormScreen extends React.Component{
    
    resetState = () => {
        fetch("https://ug-app.azurewebsites.net").then(
            (res) => {
                return res.json()
            }
                ).then(
            (res) => {
                this.setState({
                    forms: res,
                    choosenForm: undefined
                })
            })
    }
    
    constructor(){
        super();
        
        const initialState = {
            forms: [],
            choosenForm: undefined   
        }

        this.state = initialState

    }

    setChoosenForm = async(formId) => {
        const response = await fetch("https://ug-app.azurewebsites.net",
        {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                id: this.state.forms[formId].id
            })
        }
        )


        await this.resetState()
    }

    componentDidMount = () => {
        this.resetState()
    }

    render() {
        return (
            <NativeBaseProvider>
                <ShowFormScreen 
                    forms = {this.state.forms}
                    choose = {this.setChoosenForm}  
                />
            </NativeBaseProvider>
        )
    }
}

export default DeleteFormScreen;