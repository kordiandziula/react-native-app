import { StatusBar } from 'expo-status-bar';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { registerRootComponent } from 'expo';

import AddFormScreen from './screens/AddFormScreen';
//import ShowFormScreen from './screens/ShowFormScreen';
import EditFormScreen from './screens/EditFormScreen';
import ResolveFormScreen from './screens/ResolveFormScreen';
import DeleteFormScreen from './screens/DeleteFormScreen';
import ResultsFormScreen from './screens/ResultsFormScreen';


import { Icon } from 'react-native-elements';
import { LogBox } from "react-native";
LogBox.ignoreLogs([""]);

const Tab = createBottomTabNavigator();


export default function App() {
  return (
    <NavigationContainer>
        
        <Tab.Navigator screenOptions={{
            tabBarActiveTintColor: "red",
            tabBarShowLabel: false,
            tabBarStyle: [
              {
                "display": "flex"
              },
            null
            ]}}
        >
          <Tab.Screen 
            name="DODAJ" 
            component={AddFormScreen} 
            options={{
              unmountOnBlur: true,
              tabBarIcon: ({color, size}) => (
                <Icon 
                  name="add-circle-outline"
                  type="ionicon"
                  color= {color}
                  size = {size}
                />
              )
            }}
          />

          <Tab.Screen 
            name="USUN" 
            component={DeleteFormScreen} 
            options={{
              unmountOnBlur: true,
              tabBarIcon: ({color, size}) => (
                <Icon 
                  name="close-circle-outline"
                  type="ionicon"
                  color= {color}
                  size = {size}
                />
              )
            }}
          />


          <Tab.Screen 
            name="EDYTUJ" 
            component={EditFormScreen} 
            options={{
              unmountOnBlur: true,
              tabBarIcon: ({color, size}) => (
                <Icon 
                  name="edit-2"
                  type="feather"
                  color= {color}
                  size = {size}
                />
              )
            }}
          />

          <Tab.Screen 
            name="ROZWIAZ" 
            component={ResolveFormScreen} 
            options={{
              unmountOnBlur: true,
              tabBarIcon: ({color, size}) => (
                <Icon 
                  name="edit"
                  type="feather"
                  color= {color}
                  size = {size}
                />
              )
            }}
          />

          <Tab.Screen 
            name="WYNIKI" 
            component={ResultsFormScreen} 
            options={{
              unmountOnBlur: true,
              tabBarIcon: ({color, size}) => (
                <Icon 
                  name="podium-outline"
                  type="ionicon"
                  color= {color}
                  size = {size}
                />
              )
            }}
          />

        </Tab.Navigator>
        
        <StatusBar style="auto" />
    </NavigationContainer>
  );
}

//registerRootComponent(App)